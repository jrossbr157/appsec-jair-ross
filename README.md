# Desafio AppSec

O objetivo desse desafio é analisar a capacidade do candidato em realizar uma exploração de vulnerabilidade no código de exemplo.

## Build
 ```sh
 $ cd appsec
 $ mvn clean package
 ```
 
## Run
 ```sh
 $ java -jar target/hello-challenge-0.0.1.jar
```

## Instrução
Depois que o aplicativo é iniciado, as vulnerabilidades e exposições nele podem ser testadas com os padrões de acesso à API descritos.

| URL | Detalhes |
| --- | ------- |
| http://localhost:8081/customers/1 | Returns JSON representation of Customer resource based on Id (1) specified in URL |
| http://localhost:8081/customers   | Returns JSON representation of all available Customer resources |
| http://localhost:8081/patients    | Returns JSON representation of all available patients in record |
| http://localhost:8081/account/1   | Returns JSON representation of Account based on Id (1) specified |
| http://localhost:8081/account     | Returns JSON representation of all available accounts and their details |

* Descreva as fraquezas encontradas que podem ser exploradas por ameaças, diferencial usar referências CVE e OWASP na análise. 

## Forma de entrega
Realizar o fork do nosso repositório e nos envie a url do seu projeto, seguindo as instruções apresentadas. O arquivo `ANSWERS.md` deve ser usado para descrever os métodos de exploração e as vulnerabilidades encontradas.

**Desejamos boa sorte no processo seletivo.**