**Desafio AppSec**

**Vulnerabilidades Identificadas**
---------------------------------------------------------------------------
JAVA 1.8 

| CVE Id        | CWE id | Type | Date       | Score | 
| --------------|--------|------|------------|-------|
| CVE-2020-280  | ------ |------|2020-04-15  |7.5	   |
| CVE-2019-2449 | ------ |  DoS |2019-01-16  |2.6	   |
| CVE-2019-2426 | ------ |------|2019-01-16  |4.3	   |
| CVE-2019-2422 | ------ |------|2019-01-16  |2.6	   |
| CVE-2018-11212|  369   |  DoS |2018-05-16  |4.3	   |


Apache maven  3.8.7 
| CVE Id        | CWE id | Type | Date       | Score | 										
|---------------|--------|------|------------|-------|
|CVE-2021-26291 | 346    |------|  2021-04-23|  6.4	 |	





**Quando verificado o código abaixo posso detectar algumas vulnerabilidades da OWASP**


        function loadDoc() {
            var xhttp = new XMLHttpRequest();
            xhttp.onreadystatechange = function() {
                if (this.readyState == 4 && this.status == 200) {
                    document.getElementById("output").innerHTML = this.responseText;
                    var a = JSON.parse(this.responseText);
                }
            };
            var custno = "customers/"+document.getElementById("searchvalue").value;
            xhttp.open("GET", custno, true);
            xhttp.send();
        }
        $("input")[0].oninput = function () {
           loadDoc();
    };
 


-> **Cross-Site Request Forgery/Broken Access Control:**  O funcionamento normal da página é mostrar os dados dos clientes de acordo com o ID inserido no campo de pesquisa, mas é possível acessar outras informações ao realizar um cross-Site Request manipulando os cabeçalhos da solicitação. Com o BurpSuite, alterei de "GET /customers/1 HTTP/1.1" para "GET /patients HTTP/1.1" ou "GET /account/1 HTTP/1.1", permitindo assim o acesso não autorizado a dados confidenciais.

->	**Sensitive Data Exposure:** Os dados sensíveis do cliente ficam expostos na requisição do json. Incluindo informações como endereço completo, número de telefone, número de Seguridade Social, data de nascimento e dados bancários. Dados que podem ser utilizados com fins maliciosos como phishing, roubo de identidade,fraude,etc. Violando a privacidade e a segurança do cliente.

->	**Injection:** Se o conteúdo da variável "custno" é controlado pelo usuário e não é validado ou sanitizado adequadamente, ela permite que um atacante injete comandos maliciosos na URL da requisição GET.

->	**XSS (Cross-Site Scripting):** Permite que insiram códigos maliciosos em uma página web caso o conteúdo da resposta não seja limpo ou codificado corretamente.


**Algumas possíveis falhas na lógica desse código que podem ser exploradas**

->	O código não trata erros de forma adequada. Se a resposta não tiver o status 200 ou o readyState não for 4, ele não vai considerar essa exceção e pode causar problema no fluxo do programa. Quando o campo fica vazio, permito acesso a todos os dados do cliente;

->	Ele atualiza o conteúdo de uma seção "output" do documento HTML sempre que o valor do campo "searchvalue" muda. O que pode causar problemas de performance se a resposta for grande ou se o campo for modificado várias vezes rapidamente;

-> 	Ele está tratando de forma geral os possíveis status code retornado, sem considerar casos específicos. Acredito ser importante corrigir isso de forma específica para melhorar o fluxo da aplicação.

