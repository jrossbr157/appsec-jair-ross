package io.challenge.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import io.challenge.model.Customer;

public interface CustomerRepository extends CrudRepository<Customer, Long> {
	public List<Customer> findByFirstName(String firstName);
}
